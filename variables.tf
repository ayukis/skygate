variable "name" {
    default = "skygate"
}

variable "region" {
    default = "us-east-1"
}

variable "access_key" {
    default = "xxxxxxxxxxx"
}

variable "secret_key" {
    default = "xxxxxxxxxxxxxx"
}

variable "az" {
    default = "us-east-1a"
}

/*variable "ssh_key_name" {
    default = "atlas"
*/}

variable "vpc_cidr_block" {
    default = "10.136.0.0/16"
}

variable "dmz_cidr_block" {
    default = "10.136.1.0/24"
}

variable "main_instance_private_ip" {
    default = "10.136.1.39"
}

